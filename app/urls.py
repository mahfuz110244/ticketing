from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static

from main.views import IndexPageView

admin.site.site_header = 'Misfit Admin'                    # default: "Django Administration"
admin.site.index_title = 'Misfit Administration'           # default: "Site administration"
admin.site.site_title = 'Misfit Online Request Management' # default: "Django site admin"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', IndexPageView.as_view(), name='index'),
    path('i18n/', include('django.conf.urls.i18n')),
    path('accounts/', include('accounts.urls')),
    path('erequest/', include('erequest.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
