import os
from .local_settings import IS_PRODUCTION

# IS_PRODUCTION = os.environ.get('IS_PRODUCTION')
IS_PRODUCTION = IS_PRODUCTION

if IS_PRODUCTION:
    from .conf.production.settings import *
else:
    from .conf.development.settings import *
