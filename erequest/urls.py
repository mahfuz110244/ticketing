from django.urls import path

from .views import *
from .pdf_view import *

app_name = 'erequest'

urlpatterns = [
    path('list', ErequestListView.as_view(), name='erequest_post_list'),
    path('view/<int:pk>', ErequestDetailView.as_view(), name='erequest_post_detail'),
    path('new', ErequestCreateView.as_view(), name='erequest_post_create'),
    path('edit/<int:pk>', ErequestUpdateView.as_view(), name='erequest_post_update'),
    path('delete/<int:pk>', ErequestDeleteView.as_view(), name='request_post_delete'),

    path('details/view/<int:pk>', ErequestDetailsDetailView.as_view(), name='erequest_details_post_detail'),
    path('details/delete/<int:pk>', ErequestDetailsDeleteView.as_view(), name='request_details_post_delete'),

    path('pdf', html_to_pdf_view, name='request_pdf_download'),

]
