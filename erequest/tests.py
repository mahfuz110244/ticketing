from django.test import TestCase
from erequest.models import Erequest, Erequest_Details
from accounts.models import User
# Create your tests here.
class ErequestTestCase(TestCase):
    def setUp(self):
        user1 = User.objects.create_user(username= "mahfuz1",
                            email= "mahfuz@misfit.tech",
                            first_name="Mahfuzur Rahman",
                            last_name="Khan",
                            password="mss@12345",
                            created_by="admin",
                            updated_by="admin",
                            )
        Erequest.objects.create(user_id = user1,
                                status= "Open",
                                hr_comments= "",
                                management_comments="",
                                subject="Applican for seak leave 1",
                                description="I am suffering from fever",
                                file="",
                                created_by="admin",
                                updated_by="admin",
                            )
        Erequest.objects.create(user_id = user1,
                                status="HR Reviewed",
                                hr_comments="Take Rest and all the best",
                                management_comments="",
                                subject="Applican for seak leave 2",
                                description="I am suffering from fever 2",
                                file="",
                                created_by="admin",
                                updated_by="admin",
                                )

        Erequest.objects.create(user_id = user1,
                                status="Processed",
                                hr_comments="Take Rest and all the best",
                                management_comments="",
                                subject="Applican for seak leave 3",
                                description="I am suffering from fever 3",
                                file="",
                                created_by="admin",
                                updated_by="admin",
                                )

    # User can create as it has valid domain
    def test_user_create_valid_domain(self):
        """User can create"""
        user2 = User.objects.create_user(username="mahfuz2",
                                         email="mahfuz2@misfit.tech",
                                         first_name="Mahfuzur Rahman",
                                         last_name="Khan",
                                         password="mss@12345",
                                         created_by="admin",
                                         updated_by="admin",
                                         )
        self.assertEqual(user2.__str__(), 'mahfuz2')

    # User can not create as it has not valid domain
    def test_user_create_not_valid_domain(self):
        """User can not create"""
        user3 = User.objects.create_user(username="mahfuz3",
                                         email="mahfuz3@gmail.com",
                                         first_name="Mahfuzur Rahman",
                                         last_name="Khan",
                                         password="mss@12345",
                                         created_by="admin",
                                         updated_by="admin",
                                         )
        self.assertEqual(user3.__str__(), 'mahfuz3')

    # Employee can request
    def test_request_create(self):
        """Request can create"""
        erequest1 = Erequest.objects.get(subject="Applican for seak leave 1")
        erequest2 = Erequest.objects.get(subject="Applican for seak leave 2")
        erequest3 = Erequest.objects.get(subject="Applican for seak leave 3")

        self.assertEqual(erequest1.__str__(), 'Applican for seak leave 1')
        self.assertEqual(erequest2.__str__(), 'Applican for seak leave 2')
        self.assertNotEqual(erequest3.__str__(), 'Applican for seak leave 4')
