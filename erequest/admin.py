from django.contrib import admin
from .models import Erequest, Erequest_Details

# Register your models here.
class ErequestAdmin(admin.ModelAdmin):
    list_display = ["id", "created_by", "created_date", "subject", "status", "updated_date", "updated_by"]

admin.site.register(Erequest, ErequestAdmin)


class ErequestDetailsAdmin(admin.ModelAdmin):
    list_display = ["erequest_id", "status"]

admin.site.register(Erequest_Details, ErequestDetailsAdmin)