from django import forms
from .models import Erequest, Erequest_Details
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User, Group, GroupManager
from django.contrib import admin


class ErequestForm(forms.ModelForm):
    class Meta:
        model = Erequest
        fields = ['subject','description','file','status',
                  'hr_comments', 'management_comments',
                 ]

    subject = forms.CharField(label=_('Subject'), help_text=_('Required. Give your request subject'), required=True)
    description = forms.CharField(widget=forms.Textarea, label=_('Description'), help_text=_('Required. Give your request details'),required=True)
    file = forms.FileField(required=False)
    hr_comments = forms.CharField(widget=forms.Textarea, label=_('HR Comments'), required=False)
    management_comments = forms.CharField(widget=forms.Textarea, label=_('Higher Management Comments'), required=False)
    OPEN = 'Open'
    HR_REVIEWED = 'HR Reviewed'
    PROCESSED = 'Processed'
    STATUS_CHOICES = (
        (OPEN, 'Open'),
        (HR_REVIEWED, 'HR Reviewed'),
        (PROCESSED, 'Processed')
    )
    status = forms.ChoiceField(choices=STATUS_CHOICES, label=_('Give request status'), initial=OPEN, required=True)

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)  # pop the 'user' from kwargs dictionary
        super(ErequestForm, self).__init__(*args, **kwargs)

        # Remove some field based on user
        if user.user_type=="Employee":
            fields_to_delete = ['status','hr_comments', 'management_comments' ]
        else:
            fields_to_delete = []
        if fields_to_delete:
            for field in fields_to_delete:
                del self.fields[field]

        # Set read only base on user
        if user.user_type=="HR":
            self.fields['subject'].widget.attrs['readonly'] = True
            self.fields['description'].widget.attrs['readonly'] = True
            self.fields['file'].widget.attrs['readonly'] = True
            self.fields['management_comments'].widget.attrs['readonly'] = True

        if user.user_type=="Management":
            self.fields['subject'].widget.attrs['readonly'] = True
            self.fields['description'].widget.attrs['readonly'] = True
            self.fields['file'].widget.attrs['readonly'] = True
            self.fields['hr_comments'].widget.attrs['readonly'] = True


class ErequestViewForm(forms.ModelForm):
    class Meta:
        model = Erequest
        fields = ['subject', 'description', 'status', 'file',
                  'hr_comments', 'management_comments',
                  ]

    def __init__(self, *args, **kwargs):
        super(ErequestViewForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        fields_to_delete = []
        if instance.user_id.user_type=="Employee" and instance.status=="Open":
            fields_to_delete = ['status', 'hr_comments', 'management_comments']

        if fields_to_delete:
            for field in fields_to_delete:
                del self.fields[field]

        if instance and instance.id:
            for i in self.fields:
                self.fields[i].widget.attrs['readonly'] = True


class ErequestDetailViewForm(forms.ModelForm):
    class Meta:
        model = Erequest_Details
        fields = ['subject', 'description', 'status', 'file',
                  'hr_comments', 'management_comments',
                  ]

    def __init__(self, *args, **kwargs):
        super(ErequestDetailViewForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.id:
            for i in self.fields:
                self.fields[i].widget.attrs['readonly'] = True