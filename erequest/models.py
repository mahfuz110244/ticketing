from django.db import models
from accounts.models import CommonModel, User

class Erequest(CommonModel):
    user_id = models.ForeignKey(User, related_name='erequests_user', on_delete=models.CASCADE, blank=False, verbose_name='Request User')
    subject = models.CharField("Subject", max_length=50, blank=False)
    description = models.TextField("Description", max_length=500, blank=False)
    OPEN = 'Open'
    HR_REVIEWED = 'HR Reviewed'
    PROCESSED = 'Processed'
    STATUS_CHOICES = (
        (OPEN, 'Open'),
        (HR_REVIEWED, 'HR Reviewed'),
        (PROCESSED, 'Processed')
    )
    status = models.CharField(
        max_length=20,
        choices=STATUS_CHOICES,
        default=OPEN,
    )
    hr_comments = models.TextField("HR Comments", max_length=500, blank=True, null=True, default="")
    management_comments = models.TextField("Higher Management Comments", max_length=500, blank=True, null=True, default="")
    file = models.FileField(upload_to='uploads/file/%Y/%m/', blank=True, null=True)

    def save(self, *args, **kwargs):
        super(Erequest, self).save(*args, **kwargs)
        Erequest_Details.objects.create(
                                                    erequest_id=self,
                                                    status= self.status,
                                                    hr_comments= self.hr_comments if self.hr_comments is not None else "",
                                                    management_comments=self.management_comments if self.management_comments is not None else "",
                                                    subject=self.subject,
                                                    description=self.description,
                                                    file=self.file if self.file is not None else "",
                                                    created_by=self.created_by,
                                                    updated_by=self.updated_by,
                                                    created_date=self.created_date,
                                                    updated_date=self.updated_date)

    class Meta:
        ordering = ["-id", "-user_id", "-status"]
        verbose_name = 'Employee Request'

    def __str__(self):  # __unicode__ on Python 2
        return self.subject

class Erequest_Details(CommonModel):
    erequest_id = models.ForeignKey(Erequest, related_name='erequests', on_delete=models.CASCADE, blank=False, verbose_name='Request ID')
    subject = models.CharField("Subject", max_length=50, blank=False, default="")
    description = models.TextField("Description", max_length=500, blank=False, default="")
    OPEN = 'Open'
    HR_REVIEWED = 'HR Reviewed'
    PROCESSED = 'Processed'
    STATUS_CHOICES = (
        (OPEN, 'Open'),
        (HR_REVIEWED, 'HR Reviewed'),
        (PROCESSED, 'Processed')
    )
    status = models.CharField(
        max_length=20,
        choices=STATUS_CHOICES,
        default=OPEN,
    )
    hr_comments = models.TextField("HR Comments", max_length=500, blank=False, default="")
    management_comments = models.TextField("Higher Management Comments", max_length=500, blank=False, default="")
    file = models.FileField(upload_to='uploads/file/%Y/%m/', blank=True, null=True)

    class Meta:
        ordering = ["-id", "-erequest_id",]
        verbose_name = 'Employee Request Detail'

    def __str__(self):  # __unicode__ on Python 2
        return str(self.id) + "---" + str(self.subject)