# Download request list as pdf
from .models import Erequest
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.template.loader import render_to_string

from weasyprint import HTML

def html_to_pdf_view(request):
    template_name = 'erequest/request_pdf.html'
    status = request.GET.get('status', "")
    object_list = Erequest.objects.filter(status=status)
    title = status + " Requests List"
    html_string = render_to_string(template_name, {'title': title, 'object_list': object_list})

    html = HTML(string=html_string)
    html.write_pdf(target='/tmp/mypdf.pdf');

    fs = FileSystemStorage('/tmp')
    with fs.open('mypdf.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="requests.pdf"'
        return response

    return response