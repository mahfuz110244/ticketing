# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import ErequestForm, ErequestViewForm, ErequestDetailViewForm
from .models import Erequest, Erequest_Details
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.http import Http404
from django.utils.translation import gettext_lazy as _

# Get All Request list
class ErequestListView(LoginRequiredMixin, ListView):
    model = Erequest
    template_name = 'erequest/post_list.html'
    success_url = "/erequest/list"

    def get_queryset(self):
        status = self.request.GET.get('status', "")
        if status:
            if self.request.user.user_type=="Employee":
                return Erequest.objects.filter(user_id=self.request.user, status=status)
            elif self.request.user.user_type=="HR":
                return Erequest.objects.filter(status=status)
            elif self.request.user.user_type=="Management":
                return Erequest.objects.filter(status=status)
            return Erequest.objects.all()
        else:
            if self.request.user.user_type=="Employee":
                return Erequest.objects.filter(user_id=self.request.user)
            elif self.request.user.user_type=="HR":
                return Erequest.objects.filter(status='Open')
            elif self.request.user.user_type=="Management":
                return Erequest.objects.filter(status='HR Reviewed')
            return Erequest.objects.all()

    def get_context_data(self, **kwargs):
        status = self.request.GET.get('status', "")
        context = super().get_context_data(**kwargs)
        context['status'] = status
        return context


# Create New Request
class ErequestCreateView(LoginRequiredMixin, CreateView):
    model = Erequest
    template_name = 'erequest/post_form.html'
    success_url = "/erequest/list"
    form_class = ErequestForm

    def get_form_kwargs(self):
        kwargs = super(ErequestCreateView, self).get_form_kwargs()
        kwargs['user'] = self.request.user  # pass the 'user' in kwargs
        return kwargs

    def form_invalid(self, form):
        return super(ErequestCreateView, self).form_invalid(form)

    def form_valid(self, form):
        form.instance.user_id = self.request.user
        form.instance.created_by = self.request.user
        form.instance.updated_by = self.request.user
        messages.success(self.request, _('Request has been created successfully.'))
        return super().form_valid(form)


# Update Request
class ErequestUpdateView(LoginRequiredMixin, UpdateView):
    model = Erequest
    template_name = 'erequest/post_update.html'
    form_class = ErequestForm
    success_url = "/erequest/list"

    def get_form_kwargs(self):
        kwargs = super(ErequestUpdateView, self).get_form_kwargs()
        kwargs['user'] = self.request.user  # pass the 'user' in kwargs
        return kwargs

    def form_valid(self, form):
        form.instance.updated_by = self.request.user.username
        messages.success(self.request, _('Request data has successfully updated.'))
        return super().form_valid(form)

# Details Request
class ErequestDetailView(LoginRequiredMixin, UpdateView):
    model = Erequest
    template_name = 'erequest/post_detail.html'
    form_class = ErequestViewForm


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        erequest_details_obj = Erequest_Details.objects.filter(erequest_id=self.kwargs['pk'])
        context['erequest_details_obj'] = erequest_details_obj
        return context

# Delete Request
class ErequestDeleteView(LoginRequiredMixin, DeleteView):
    model = Erequest
    template_name = 'erequest/post_delete.html'
    success_url = "/erequest/list"

    def get_object(self, queryset=None):
        """ Hook to ensure object is owned by request.user. """
        obj = super(ErequestDeleteView, self).get_object()
        if not obj.user_id  == self.request.user:
            messages.success(self.request, _('Unauthorized Access.'))
            raise Http404
        else:
            return obj


# Details view of Request Details
class ErequestDetailsDetailView(LoginRequiredMixin, UpdateView):
    model = Erequest_Details
    template_name = 'erequest_details/post_detail.html'
    form_class = ErequestDetailViewForm


# Delete Request Details
class ErequestDetailsDeleteView(LoginRequiredMixin, DeleteView):
    model = Erequest_Details
    template_name = 'erequest_details/post_delete.html'
    success_url = "/erequest/list"

    def get_object(self, queryset=None):
        """ Hook to ensure object is owned by request.user. """
        obj = super(ErequestDetailsDeleteView, self).get_object()
        if not obj.erequest_id.user_id  == self.request.user:
            messages.success(self.request, _('Unauthorized Access.'))
            raise Http404
        else:
            return obj