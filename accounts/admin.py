from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from .models import User
from django import forms
from django.utils.translation import gettext_lazy as _


# Re-register UserAdmin
# admin.site.unregister(User)
admin.site.register(User, UserAdmin)

class UserCreateForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('username', 'first_name' , 'last_name','email', 'user_type')
        # fields = "__all__"
    username = forms.CharField(label=_('Username'), help_text=_('Required. Enter your username.'))
    email = forms.EmailField(label=_('Email'), help_text=_('Required. Enter an new email address.'))

    def clean_email(self):
        data = self.cleaned_data['email']
        domain = data.split('@')[1]
        domain_list = ["misfit.tech"]
        if domain not in domain_list:
            raise forms.ValidationError("Please enter an Email Address with a valid domain. Email under misfit.tech domain are only allowed.")
        return data

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user


class UserAdmin(UserAdmin):
    add_form = UserCreateForm
    list_display = ["username", "email", "user_type", "is_active"]
    # prepopulated_fields = {'username': ('first_name' , 'last_name', )}

    fieldsets = (
        (None, {'fields': ('user_type',)}),
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('user_type', 'username',('first_name', 'last_name'),  'email', 'password1', 'password2', ),
        }),
    )
# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
