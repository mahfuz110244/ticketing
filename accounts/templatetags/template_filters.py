# coding=utf-8
from django import template
from accounts.models import User

register = template.Library()

@register.filter(name='check_user_type')
def check_user_type(login_user_type, given_user_type):
    if login_user_type == given_user_type:
        return True
    return False

@register.filter(name='check_status')
def check_status(status, new_status):
    if str(status)==str(new_status):
        return True
    return False